
# Facial Detection

A C++ class and library that will interface Qt with OpenCV.
This class will be used to perform the facial detection code necessary to complete Project 3.
In addition, there are two non-member functions (Ones that can be called outside of classes) that
will assist with integrating OpenCV's images with the Qt file system.

All topics covered within this library are described in excruciating detail within `facialdetection.cpp`, make sure to read it all and ask questions as they arise!


## Installation:
To use these files, clone this repository to a directory within your VM. You will be using the `facialdetection.cpp/h` within your server project, so make sure to copy these two files over to your project folder.

This means that if we have our project 3 server code within the folder ~/Documents/Project3Server, we would follow the below procedure:
```bash
	cd ~/Documents
	git clone https://gitlab.com/SP_Silicon/facialdetection.git
	cd facialdetection
	cp facialdetection.cpp ../Project3Server
	cp facialdetection.h ../Project3Server
```

## Use case:
This library contains both a class and extraneous public functions. Example use cases of them are seen below:

```c++
	cv::Mat image = qrcimread(":/qrc/path/to/image.png");
	cv::CascadeClassifier cascade = qrcCascade(":/qrc/path/to/cascade.xml");
	QString border_path(":/qrc/path/to/border/image.png");
	FacialDetection fd(cascade, border_path);
	size_t face_count{};
	
	cv::Mat detectedFaces = fd.detectAndDisplay(image, &face_count);
	if (face_count != 0) {
		face_vec faceLocations = fd.getFaces();
		// Do computation with the bounding boxes of the faces.
	}
	cv::imshow("Hello", detectedFaces);
```
This example program will open an image, detect faces within the image, and display the outcome within OpenCV's built-in image viewer. Both `qrcimread` and `qrcCascade` rely on their corresponding files to exist within the projects QRC file, and the FacialDetection class requires the border image to exist within the QRC file as well.

A QRC file plus resources will be provided to the users through the resources folder provided in the project document.
