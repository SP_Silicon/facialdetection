#include "facialdetection.h"


/*
 * FacialDetection:
 * A C++ class and library that will interface Qt with OpenCV.
 *
 * This class will be used to perform the facial detection code necessary to complete Project 3.
 *
 * In addition, there are two non-member functions (Ones that can be called outside of classes) that
 * will assist with integrating OpenCV's images with the Qt file system.
 *
 *
 * All topics covered within this library are described in excrutiating detail below, so please read it all.
 *
 */


/*
 *
 * FacialDetection constructor:
 * The FacialDetection class takes one parameter, and has three private variables:
 *
 * Parameters:
 *      cas: The facial detection cascade that will be used within the detection code.
 *
 * Variables:
 *      cascade: The private location where the cascade parameter cas can be accessed.
 *      border: A OpenCV Mat that will contain the border image that will be overlayed on detected faces.
 *      mask: An OpenCV Mat that will contain the mask away all transparent areas of the border, allowing
 *            for proper overlaying of the border on the scene.
 *
 *
 *
 * Example call:
 *  FacialDetection faceDetect(<cascade_i_want_to_use>);
 *
 */
FacialDetection::FacialDetection(cv::CascadeClassifier& cas, const QString& borderUrl) : cascade(cas) {
    border = qrcimread(borderUrl, cv::IMREAD_UNCHANGED); // Open Overlayed image
    mask = getAlphaLevel(border);
}

/*
 * detectAndDisplay:
 * A function that will detect the number of faces within an frame
 *
 * This function take in a single frame of a video object as a parameter,
 * and will take use of a cascade classifier to detect faces within the image.
 *
 * After detecting a face, detectAndDisplay will draw around the face it detects a bounding box.
 * Finally it will return a new Mat containing the updated image with the bounding boxes drawn,
 * and the face_count parameter updated.
 *
 * This is a member function, which means it must be called on a FacialDetection object
 *
 * Example call:
 *  size_t faces{};
 *  FacialDetection faceDetect(cascade_we_want_to_use);
 *  cv::Mat frame = faceDetect.detectAndDisplay(input_frame, &faces);
 *
 * This function takes the Mat we want to find a face in and the number of faces variable as a parameter.
 */
cv::Mat FacialDetection::detectAndDisplay(cv::Mat& frame, size_t* face_count) {
    cv::Mat outFrame = frame.clone();
    cv::Mat frame_gray;
    cv::Mat bcopy;
    cv::Mat mcopy;
    cv::cvtColor( frame, frame_gray, cv::COLOR_BGR2GRAY );
    cv::equalizeHist( frame_gray, frame_gray );
    //-- Detect faces
    // -- Parameters you can change for performance adjustment:
    double scaleFactor = 1.2;
    int minNeighbors = 2;

    cascade.detectMultiScale(frame_gray, faces, scaleFactor, minNeighbors);
    *face_count = faces.size();

    for ( size_t i = 0; i < faces.size(); i++ )
    {
        cv::resize(border, bcopy, cv::Size(faces[i].width, faces[i].height));
        cv::resize(mask, mcopy, cv::Size(faces[i].width, faces[i].height));

        bcopy.copyTo(outFrame(faces[i]), mcopy);
    }
    //-- Show what you got
    return outFrame;
}

/*
 * getFaces:
 * A function that will return the internal list of the locations where faces were detected after detectAndDisplay is ran.
 *
 * This function is a simple getter, but it has some interesting aspects to it described below
 *
 * The return type of this function is a face_vec. This is a redefinition of std::vector<cv::Rect>,
 * and contains all of the same member functions that a normal std::vector has. For an understanding of cv::Rect,
 * check out this website: https://docs.opencv.org/4.5.0/d2/d44/classcv_1_1Rect__.html.
 *
 * A cv::Rect datatype has 4 fields we care about:
 *  x: The top left x coordinate of the bounding Rect.
 *  y: The top left y coordinate of the bounding Rect.
 *  width: The width of the bounding Rect.
 *  height: The height of the bounding Rect.
 *
 *  These 4 fields can be used to find the location within the image that the face is detected.
 *
 * This is a member function, which means it must be called on a FacialDetection object
 *
 * Example call:
 *  size_t faces{};
 *  FacialDetection faceDetect(cascade_we_want_to_use);
 *  cv::Mat frame = faceDetect.detectAndDisplay(input_frame, &faces);
 *  if (faces != 0) {
 *      face_vec faceLocation = faceDetect.getFaces();
 *      std::cout << faceLocation[0].x << faceLocation[0].y << faceLocation[0].width << faceLocation[0].height << std::endl;
 *  }
 *
 */
face_vec FacialDetection::getFaces() {
    return faces;
}

/*------------------------------------------------------------------------- Private helper functions: -------------------------------------------------------------------------*/


/*
 * getAlphaLevel:
 * A function to return a mask of the transparent aspects of a Mat.
 *
 * This function take in a Mat and retrieve it's alpha level from it.
 * This alpha level will be used to create a mask of the see-through areas
 * allowing for images to truly appear transparent when overlayed on other Mats.
 *
 * Example call:
 *  cv::Mat mask = getAlphaLevel(Mat_With_Transparency);
 *
 * This function takes just the Mat we want to pull the alpha channel out from as a parameter.
 */
cv::Mat FacialDetection::getAlphaLevel(cv::Mat& input) {
    cv::Mat mask;
    std::vector<cv::Mat> rgbLayer;
    cv::split(input, rgbLayer);
    if(input.channels() == 4) {
        cv::split(input,rgbLayer);         // seperate channels
        cv::Mat cs[3] = { rgbLayer[0],rgbLayer[1],rgbLayer[2] };
        cv::merge(cs,3,input);  // glue together again
        mask = rgbLayer[3];       // png's alpha channel used as mask
    }
    return mask;
}

/*------------------------------------------------------------------------- Non-member public functions: -------------------------------------------------------------------------*/


/*
 * qrcimread:
 * A function to replace the imread function within opencv.
 *
 * This function will allow you to create Mats from a file contained within
 * a QRC file.
 *
 * Example call:
 *  cv::Mat test = qrcimread(":/qrc/path/to/image.png")
 *
 * This function will take all of the same flags as imread does
 */
cv::Mat qrcimread(const QString& qrc, const int flag)
{
    QFile file(qrc);
    cv::Mat m;
    if(file.open(QIODevice::ReadOnly))
    {
        QTemporaryFile temp;
        if(temp.open()) {
            temp.write(file.readAll());
            temp.close();
            m = cv::imread(temp.fileName().toStdString(), flag);
        }
    }
    return m;
}


/*
 * qrcCascade:
 * A function to to replace OpenCV's default loading of cascades.
 *
 * This function will allow you to create Cascades from a file contained within
 * a QRC file.
 *
 * Example call:
 *  cv::CascadeClassifier cascade = qrcCascade(":/qrc/path/to/cascade.xml");
 *
 * This if the load fails, it will return a nullptr instead of the cascadeClassifer
 */
cv::CascadeClassifier qrcCascade(const QString& qrc_path) {
    cv::CascadeClassifier cascade;
    QFile xml(qrc_path);
    if(xml.open(QFile::ReadOnly | QFile::Text)) {
        QTemporaryFile temp;
        if(temp.open()) {
            temp.write(xml.readAll());
            temp.close();
            if(!cascade.load(temp.fileName().toStdString())) {
                std::cout << "--(!)Error loading face cascade\n";
            }
        }
    }
    return cascade;
}

