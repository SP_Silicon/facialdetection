#ifndef FACIALDETECTION_H
#define FACIALDETECTION_H

#include <opencv4/opencv2/opencv.hpp>
#include <QString>
#include <QTemporaryFile>

using face_vec = std::vector<cv::Rect>;
class FacialDetection
{
public:
    FacialDetection(cv::CascadeClassifier&, const QString&);
    cv::Mat detectAndDisplay(cv::Mat&, size_t*);
    face_vec getFaces();
private:
    cv::Mat getAlphaLevel(cv::Mat&);
    cv::CascadeClassifier cascade{};
    cv::Mat border{};
    cv::Mat mask{};
    face_vec faces{};
};

cv::CascadeClassifier qrcCascade(const QString&);
cv::Mat qrcimread(const QString&, const int flag = cv::IMREAD_COLOR);
#endif // FACIALDETECTION_H
